package challenge_2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestXORHexStrings(t *testing.T) {
	Assert := assert.New(t)

	a := "1c0111001f010100061a024b53535009181c"
	b := "686974207468652062756c6c277320657965"
	expected := "746865206b696420646f6e277420706c6179"

	output := XORHexStrings(a, b)
	Assert.Equal(expected, output)
}
