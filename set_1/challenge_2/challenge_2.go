package challenge_2

import (
	"encoding/hex"
)

func XORHexStrings(a, b string) string {
	aBytes, _ := hex.DecodeString(a)
	bBytes, _ := hex.DecodeString(b)

	outBytes := XORBytes(aBytes, bBytes)

	return hex.EncodeToString(outBytes)
}

func XORBytes(a, b []byte) []byte {
	outBytes := make([]byte, len(a))
	for i := 0; i < len(a); i++ {
		outBytes[i] = a[i] ^ b[i]
	}
	return outBytes
}
