package challenge_3

import (
	"strings"
)

type ScoredString struct {
	String  string
	KeyChar byte
	Score   float32
}

func GetSingleCharXORScores(b []byte) []ScoredString {

	results := []ScoredString{}
	for char := 0; char < 256; char++ {
		unXored := make([]byte, len(b))
		for i := 0; i < len(b); i++ {
			unXored[i] = b[i] ^ byte(char)
		}

		results = append(results, ScoredString{
			String:  string(unXored),
			KeyChar: byte(char),
		})
	}

	for i := 0; i < len(results); i++ {
		results[i].Score = GetStringScore(results[i].String)
	}

	return results
}

func GetStringScore(s string) float32 {
	letterScores := map[string]float32{
		"a": 8.497,
		"b": 1.492,
		"c": 2.202,
		"d": 4.253,
		"e": 11.162,
		"f": 2.228,
		"g": 2.015,
		"h": 6.094,
		"i": 7.546,
		"j": 0.153,
		"k": 1.292,
		"l": 4.025,
		"m": 2.406,
		"n": 6.749,
		"o": 7.507,
		"p": 1.929,
		"q": 0.095,
		"r": 7.587,
		"s": 6.327,
		"t": 9.356,
		"u": 2.758,
		"v": 0.978,
		"w": 2.560,
		"x": 0.150,
		"y": 1.994,
		"z": 0.077,
		" ": 5,
	}
	var score float32 = 0
	for _, letter := range s {
		lower := strings.ToLower(string(letter))
		if freq, ok := letterScores[lower]; ok {
			score += freq
		}
	}
	return score
}

func GetTopScore(scores []ScoredString) ScoredString {
	var highScore float32 = 0
	var highScoredString ScoredString
	for _, score := range scores {
		if score.Score > highScore {
			highScore = score.Score
			highScoredString = score
		}
	}

	return highScoredString
}
