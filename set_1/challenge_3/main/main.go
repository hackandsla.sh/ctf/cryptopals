package main

import (
	"encoding/hex"
	"fmt"

	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_3"
)

func main() {

	s := "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
	b, _ := hex.DecodeString(s)

	scores := challenge_3.GetSingleCharXORScores(b)

	fmt.Println(challenge_3.GetTopScore(scores).String)
}
