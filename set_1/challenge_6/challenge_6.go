package challenge_6

import "math"

func GetHammingDistance(a, b []byte) int {
	count := 0
	for i := 0; i < len(a); i++ {
		diff := a[i] ^ b[i]
		for j := 0; j < 8; j++ {
			if diff&1 == 1 {
				count++
			}
			diff = diff >> 1
		}
	}
	return count
}

// GetLikelyKeySizes retrieves the top n likely keysizes from byte array b
func GetLikelyKeySizes(b []byte, n, numSamples int) []int {
	maxKeySize := 40
	distances := map[int]float32{}
	for blockSize := 1; blockSize <= maxKeySize; blockSize++ {
		distance := 0
		for sample := 0; sample < numSamples; sample++ {
			start1 := blockSize * sample
			end1 := start1 + blockSize

			start2 := end1
			end2 := start2 + blockSize
			distance += GetHammingDistance(b[start1:end1], b[start2:end2])
		}

		weightedDistance := float32(distance) / float32(blockSize)
		distances[blockSize] = weightedDistance
	}

	ret := make([]int, n)
	for i := 0; i < n; i++ {
		var lowestDistance float32 = math.MaxFloat32
		lowestDistanceKeySize := 0

		for keySize, distance := range distances {
			if distance < lowestDistance {
				lowestDistance = distance
				lowestDistanceKeySize = keySize
			}
		}

		ret[i] = lowestDistanceKeySize
		delete(distances, lowestDistanceKeySize)
	}
	return ret
}
