package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"

	"log"

	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_3"
	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_6"
)

func main() {
	encodedMessage, err := ioutil.ReadFile("encrypted.txt")
	if err != nil {
		log.Fatalf("%v", err)
	}

	encryptedMessage, err := base64.StdEncoding.DecodeString(string(encodedMessage))
	if err != nil {
		log.Fatalf("%v", err)
	}

	likelyKeySizes := challenge_6.GetLikelyKeySizes(encryptedMessage, 1, 10)

	keySize := likelyKeySizes[0]
	blocks := make([][]byte, keySize)
	for i := 0; i < keySize; i++ {
		blocks[i] = []byte{}
	}

	for i := 0; i < len(encryptedMessage); i++ {
		blocks[i%keySize] = append(blocks[i%keySize], encryptedMessage[i])
	}

	textBlocks := make([]string, keySize)
	key := make([]byte, keySize)
	for i := 0; i < keySize; i++ {
		scores := challenge_3.GetSingleCharXORScores(blocks[i])
		topScoredString := challenge_3.GetTopScore(scores)
		textBlocks[i] = topScoredString.String
		key[i] = topScoredString.KeyChar
	}

	reconstructed := []byte{}
outer:
	for {
		for i := 0; i < keySize; i++ {
			if len(textBlocks[i]) == 0 {
				break outer
			}

			reconstructed = append(reconstructed, textBlocks[i][0])
			textBlocks[i] = textBlocks[i][1:]
		}
	}

	fmt.Println(string(reconstructed))
	fmt.Printf("\n\nKey: '%s'\n", string(key))
}
