package challenge_6

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetHammingDistance(t *testing.T) {
	Assert := assert.New(t)

	a := "this is a test"
	b := "wokka wokka!!!"
	expected := 37

	output := GetHammingDistance([]byte(a), []byte(b))
	Assert.Equal(expected, output)
}
