package main

import (
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

const BlockSize = 16

func main() {
	b, err := ioutil.ReadFile("lines.txt")
	if err != nil {
		log.Fatalln(err.Error())
	}

	candidates := []string{}
	lines := strings.Split(string(b), "\n")
outer:
	for _, line := range lines {
		decoded, err := hex.DecodeString(line)
		if err != nil {
			log.Fatalln(err.Error())
		}

		blocks := map[string]bool{}

		for i := 0; i < len(decoded); i += BlockSize {
			block := string(decoded[i : i+BlockSize])
			if _, ok := blocks[block]; ok {
				candidates = append(candidates, line)
				continue outer
			}

			blocks[block] = true
		}
	}

	for _, candidate := range candidates {
		fmt.Println(candidate)
	}
}
