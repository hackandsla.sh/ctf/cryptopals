package challenge_7

import (
	"encoding/base64"
	"io/ioutil"
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDecryptAES_ECB(t *testing.T) {
	Assert := assert.New(t)
	key := []byte("YELLOW SUBMARINE")

	encoded, err := ioutil.ReadFile("./encrypted.txt")
	if err != nil {
		log.Fatalf("%v", err)
	}
	expected, err := ioutil.ReadFile("./decrypted.txt")
	if err != nil {
		log.Fatalf("%v", err)
	}

	encrypted, err := base64.StdEncoding.DecodeString(string(encoded))
	if err != nil {
		log.Fatalf("%v", err)
	}

	decrypted := DecryptAES_ECB(encrypted, key)
	Assert.Equal(expected, decrypted)
}

func TestEncryptAES_ECB(t *testing.T) {
	Assert := assert.New(t)
	key := []byte("YELLOW SUBMARINE")

	decrypted, err := ioutil.ReadFile("./decrypted.txt")
	if err != nil {
		log.Fatalf("%v", err)
	}
	expectedEncoded, err := ioutil.ReadFile("./encrypted.txt")
	if err != nil {
		log.Fatalf("%v", err)
	}
	expected, err := base64.StdEncoding.DecodeString(string(expectedEncoded))
	if err != nil {
		log.Fatalf("%v", err)
	}

	encrypted := EncryptAES_ECB(decrypted, key)
	Assert.Equal(expected, encrypted)
}
