package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_7"
)

func main() {
	key := []byte("YELLOW SUBMARINE")

	encoded, err := ioutil.ReadFile("../encrypted.txt")
	if err != nil {
		log.Fatalf("%v", err)
	}

	encrypted, err := base64.StdEncoding.DecodeString(string(encoded))
	if err != nil {
		log.Fatalf("%v", err)
	}

	decrypted := challenge_7.DecryptAES_ECB(encrypted, key)

	fmt.Println(string(decrypted))
}
