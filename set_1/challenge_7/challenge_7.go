package challenge_7

import (
	"crypto/aes"
	"log"
)

func DecryptAES_ECB(b []byte, key []byte) []byte {
	cipher, err := aes.NewCipher(key)
	if err != nil {
		log.Fatalf("%v", err)
	}

	decrypted := make([]byte, len(b))
	size := cipher.BlockSize()

	for bs, be := 0, size; bs < len(b); bs, be = bs+size, be+size {
		cipher.Decrypt(decrypted[bs:be], b[bs:be])
	}

	return decrypted
}

func EncryptAES_ECB(b []byte, key []byte) []byte {
	cipher, err := aes.NewCipher(key)
	if err != nil {
		log.Fatalf("%v", err)
	}

	encrypted := make([]byte, len(b))
	size := cipher.BlockSize()

	for bs, be := 0, size; bs < len(b); bs, be = bs+size, be+size {
		cipher.Encrypt(encrypted[bs:be], b[bs:be])
	}

	return encrypted
}
