package challenge_1

import (
	"encoding/base64"
	"encoding/hex"
)

func HexToBase64(s string) string {
	b, _ := hex.DecodeString(s)
	return base64.StdEncoding.EncodeToString(b)
}
