package challenge_5

import (
	"encoding/hex"
)

func RepeatingKeyXOR(s, key string) string {
	out := make([]byte, len(s))
	for i := 0; i < len(s); i++ {
		out[i] = s[i] ^ key[i%len(key)]
	}
	return hex.EncodeToString(out)
}
