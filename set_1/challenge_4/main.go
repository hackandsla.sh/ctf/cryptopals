package main

import (
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_3"
)

func main() {
	b, err := ioutil.ReadFile("4.txt")
	if err != nil {
		log.Fatalf("%v", err)
	}

	data := string(b)
	candidates := strings.Split(data, "\n")

	allScores := []challenge_3.ScoredString{}
	for _, candidate := range candidates {
		b, _ := hex.DecodeString(candidate)

		scores := challenge_3.GetSingleCharXORScores(b)
		allScores = append(allScores, scores...)
	}

	fmt.Println(challenge_3.GetTopScore(allScores).String)
}
