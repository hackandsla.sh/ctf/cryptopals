package challenge_17

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"io/ioutil"
	"math/big"

	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_10"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_15"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_9"
)

const BlockSize = 16

type Encryptor struct {
	key     []byte
	strings [][]byte
}

func NewEncryptor(file string) (*Encryptor, error) {
	ret := &Encryptor{
		key: make([]byte, BlockSize),
	}

	_, err := rand.Read(ret.key)
	if err != nil {
		return nil, err
	}

	content, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	encoded := bytes.Split(content, []byte("\n"))
	for _, enc := range encoded {
		decoded := make([]byte, base64.StdEncoding.DecodedLen(len(enc)))
		if _, err := base64.StdEncoding.Decode(decoded, enc); err != nil {
			return nil, err
		}

		ret.strings = append(ret.strings, decoded)
	}

	return ret, nil
}

func (e *Encryptor) GetEncryptedString(i int) (ciphertext, iv []byte) {
	s := e.strings[i]

	return e.encrypt(s)
}

func (e *Encryptor) CheckPadding(ciphertext, iv []byte) bool {
	decrypted := challenge_10.Decrypt_AES_CBC(ciphertext, e.key, iv)

	// VULNERABLE!!!!!
	if err := challenge_15.ValidatePadding(decrypted); err != nil {
		return false
	}

	return true
}

func (e *Encryptor) encrypt(plaintext []byte) (ciphertext, iv []byte) {
	padded := challenge_9.PadPKCS7(plaintext, BlockSize)

	iv = make([]byte, BlockSize)
	_, _ = rand.Read(iv)

	encrypted := challenge_10.Encrypt_AES_CBC(padded, e.key, iv)

	return encrypted, iv
}

func GetRandInt(max int) int {
	i, _ := rand.Int(rand.Reader, big.NewInt(int64(max)))
	return int(i.Int64())
}
