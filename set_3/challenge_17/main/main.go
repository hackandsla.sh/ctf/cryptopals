package main

import (
	"fmt"
	"log"
	"math"

	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_9"
	"gitlab.com/terrabitz/ctf/cryptopals/set_3/challenge_17"
)

const BlockSize = challenge_17.BlockSize

func main() {
	e, err := challenge_17.NewEncryptor("../unknown.txt")
	logErr(err)

	for stringNum := 0; stringNum < 10; stringNum++ {
		encrypted, iv := e.GetEncryptedString(stringNum)

		decrypted, err := DecryptString(encrypted, iv, e)
		logErr(err)
		fmt.Println(string(decrypted))
	}
}

func logErr(err error) {
	if err != nil {
		log.Fatalf("Got error: %v\n", err)
	}
}

func DecryptString(encrypted, iv []byte, e *challenge_17.Encryptor) ([]byte, error) {
	var decrypted []byte

	encrypted = append(iv, encrypted...)

	// Work backwards from the last block. We can ignore the first block since
	// it's just the IV.
	for blockNum := (len(encrypted) / BlockSize) - 1; blockNum > 0; blockNum-- {
		blockStart := blockNum * BlockSize
		blockEnd := blockStart + BlockSize
		block := encrypted[blockStart:blockEnd]

		prevStart := blockStart - BlockSize
		prev := encrypted[prevStart:blockStart]

		// Decrypted will store the contents of the message before XORing (since
		// CBC mode requires the decrypted block to also be XORed by the content
		// of the previous encrypted block).
		decryptedBlock := make([]byte, BlockSize)

		for i := 0; i < BlockSize; i++ {
			index := BlockSize - i - 1

			// Create a deep copy of prev. However, we need the original to do
			// the final XOR.
			prevCopy := make([]byte, len(prev))
			copy(prevCopy, prev)

			// Fill the last i bytes with valid padding
			for j := 0; j < i; j++ {
				index := BlockSize - j - 1
				prevCopy[index] = decryptedBlock[index] ^ byte(i+1)
			}

			foundByte := false
			// This is the best way to cycle through all possible byte values.
			// If our iterator is a byte, then we would cycle back to 0 once we
			// incremented the max value.
			for attempt := 0; attempt <= math.MaxUint8; attempt++ {
				prevCopy[index] = byte(attempt)
				if e.CheckPadding(block, prevCopy) {
					if i == 0 {
						// If this is the first byte of the block, do an additional check to ensure that we're not
						// accidentally getting a padding success for something other than 0x1. If we modify the
						// second-to-last byte and we no longer have valid padding, then we know our attempt didn't
						// result in 0x01.
						prevCopy[index-1] += 1
						if !e.CheckPadding(block, prevCopy) {
							continue
						}
					}

					if foundByte {
						// This case should never occur, since it means that we
						// found multiple decryptions resulting in valid
						// padding. The most likely explanation for this error
						// condition is that the padding validation function is
						// buggy.
						return nil, fmt.Errorf("Found duplicate")
					}

					// Since we know that this attempt results in a valid
					// padding byte of i+1, we can figure out our pre-XORed
					// decrypted value by XORing it against the known padding
					// byte.
					decryptedBlock[index] = byte(attempt) ^ byte(i+1)
					foundByte = true
				}
			}

			if !foundByte {
				// This case should never occur, since it means we cycled
				// through every possible byte value without finding a valid
				// padding byte. The most likely explanation for this is that
				// we're not checking all values.
				return nil, fmt.Errorf("Couldn't find padding success. i=%v blockstart=%v", i, blockStart)
			}
		}

		// XOR the decrypted block by the previous block to get the final *real*
		// decryption.
		xored := make([]byte, len(decryptedBlock))
		for i := 0; i < len(xored); i++ {
			xored[i] = decryptedBlock[i] ^ prev[i]
		}

		// Since we're working backwards, prepend the newly decrypted block to
		// the rest.
		decrypted = append(xored, decrypted...)
	}

	// Assuming the original message was constructed with padding, we'll need to
	// strip it to keep our output clean.
	unpadded, err := challenge_9.UnpadPKCS7(decrypted)
	if err != nil {
		return nil, err
	}

	return unpadded, nil
}
