package challenge_18

import (
	"math"

	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_7"
)

const BlockSize = 16

func LittleEndianBytes(i uint64) []byte {
	var ret []byte

	for j := 0; j < 64/8; j++ {
		ret = append(ret, byte(i%math.MaxUint8))
		i = i >> 8
	}

	return ret
}

func GetKeyStream(i, nonce uint64, key []byte) []byte {
	nonceBytes := LittleEndianBytes(nonce)
	iBytes := LittleEndianBytes(i)

	ctr := append(nonceBytes, iBytes...)

	return challenge_7.EncryptAES_ECB(ctr, key)
}

func AESCTREncrypt(input []byte, nonce uint64, key []byte) []byte {
	var crypted []byte
outer:
	for blockNum := 0; blockNum <= len(input)/BlockSize; blockNum++ {
		keystream := GetKeyStream(uint64(blockNum), nonce, key)

		blockStart := blockNum * BlockSize
		for j := 0; j < BlockSize; j++ {
			idx := j + blockStart

			if idx >= len(input) {
				break outer
			}

			crypted = append(crypted, input[idx]^keystream[j])
		}
	}

	return crypted
}
