package challenge_18

import (
	"encoding/base64"
	"reflect"
	"testing"
)

func TestLittleEndianBytes(t *testing.T) {
	type args struct {
		i uint64
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "Convert an int to little endian bytes",
			args: args{i: 2},
			want: []byte("\x02\x00\x00\x00\x00\x00\x00\x00"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := LittleEndianBytes(tt.args.i); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LittleEndianBytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAESCTREncrypt(t *testing.T) {
	type args struct {
		nonce uint64
		key   []byte
	}
	tests := []struct {
		name        string
		args        args
		base64Input string
		want        string
	}{
		{
			args: args{
				nonce: 0,
				key:   []byte("YELLOW SUBMARINE"),
			},
			base64Input: "L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==",
			want:        "Yo, VIP Let's kick it Ice, Ice, baby Ice, Ice, baby ",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			input, err := base64.StdEncoding.DecodeString(tt.base64Input)
			if err != nil {
				t.Fatal(err)
			}

			if got := AESCTREncrypt(input, tt.args.nonce, tt.args.key); string(got) != tt.want {
				t.Errorf("AESCTREncrypt() = %v, want %v", string(got), tt.want)
			}
		})
	}
}
