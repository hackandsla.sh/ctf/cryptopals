package main

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"

	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_7"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_11"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_9"
)

const BlockSize int = 16
const RandomPrefixMax = 200

func main() {
	key := make([]byte, 16)
	_, err := rand.Read(key)
	logErr(err)

	randomPrefixLength, err := rand.Int(rand.Reader, big.NewInt(RandomPrefixMax))
	logErr(err)

	randomPrefix := make([]byte, randomPrefixLength.Int64())
	_, err = rand.Read(randomPrefix)
	logErr(err)

	unknownText, err := ioutil.ReadFile("unknown.txt")
	logErr(err)

	unknownText, err = base64.StdEncoding.DecodeString(string(unknownText))
	logErr(err)

	var oracle challenge_11.EncryptFunc = func(b []byte) []byte {
		plaintext := append(randomPrefix, b...)
		plaintext = append(plaintext, unknownText...)
		plaintext = challenge_9.PadPKCS7(plaintext, len(key))
		return challenge_7.EncryptAES_ECB(plaintext, key)
	}

	prefixEndIndex, offset, err := FindPrefixLength(oracle)
	logErr(err)

	fmt.Printf("Found start index: %v\n", prefixEndIndex)
	fmt.Printf("Found offset: %v\n", offset)

	// Fill a byte array with "A" to ensure we align our input to a block.
	align := make([]byte, offset)
	for i := 0; i < len(align); i++ {
		align[i] = 0x41
	}

	// Wrap our original oracle so we know the input always starts on a block
	// break.
	var alignedOracle challenge_11.EncryptFunc = func(b []byte) []byte {
		plaintext := append(align, b...)
		return oracle(plaintext)
	}

	c := alignedOracle([]byte{})
	// Since c is using the aligned oracle (which automatically pads the random
	// prefix with "A" till the next block start), our total message length
	// should be the total message minus the prefix length minus another block.
	messageLength := len(c) - prefixEndIndex - BlockSize
	startIndex := messageLength + prefixEndIndex
	endIndex := startIndex + BlockSize

	secretMessage := []byte{}
	for i := 0; i < messageLength-1; i++ {
		prefix := make([]byte, messageLength-i-1)
		for j := 0; j < len(prefix); j++ {
			prefix[j] = 0x41
		}

		c := alignedOracle(prefix)

		targetBlock := c[startIndex:endIndex]

		for j := byte(0); j < 255; j++ {
			guessPrefix := append(prefix, secretMessage...)
			guessPrefix = append(guessPrefix, j)
			c2 := alignedOracle(guessPrefix)

			guessBlock := c2[startIndex:endIndex]
			if bytes.Equal(targetBlock, guessBlock) {
				secretMessage = append(secretMessage, j)
				break
			}
		}
	}

	fmt.Printf("Secret message found:\n\n%s\n", string(secretMessage))
}

func logErr(err error) {
	if err != nil {
		log.Fatalf("%v", err)
	}
}

func FindPrefixLength(oracle func(b []byte) []byte) (startIndex, offset int, err error) {
	// Determine all the similar starting bytes to find which block the random
	// prefix ends in.
	i1 := oracle([]byte{})
	i2 := oracle([]byte("A"))
	index := 0
	for index = 0; index < len(i1); index++ {
		if i1[index] != i2[index] {
			break
		}
	}

	// Align it to the next smallest block start.
	prefixEndBlock := index / BlockSize
	startIndex = prefixEndBlock * BlockSize
	endIndex := startIndex + BlockSize

	// Initialize the "zero" iteration
	previousIteration := oracle([]byte{})

	// Check up to blocksize+1 (since we're already using the "zero" iteration
	// to check i==1)
	for i := 1; i <= BlockSize+1; i++ {
		// Fill an byte array with "A"
		input := make([]byte, i)
		for j := 0; j < len(input); j++ {
			input[j] = 0x41
		}

		currentIteration := oracle(input)

		// Skip past where we know the prefix is and compare the next block.
		// If it's equal between iterations, we must have hit the correct number of
		// "A"s to fill a single block.
		block1 := previousIteration[startIndex:endIndex]
		block2 := currentIteration[startIndex:endIndex]

		if bytes.Equal(block1, block2) {
			return startIndex, i - 1, nil
		}

		previousIteration = currentIteration
	}

	return 0, 0, errors.New("couldn't find the start index")
}
