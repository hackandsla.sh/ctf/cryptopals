package challenge_11

import (
	"bytes"
	"crypto/rand"
	"math/big"

	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_7"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_10"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_9"
)

const KeySize = 16

const (
	ECBMode int64 = 0
	CBCMode int64 = 1
)

func EncryptRandomModeWithAnswer(b []byte) (encrypted []byte, mode int64, err error) {

	prefixSuffixSize, _ := rand.Int(rand.Reader, big.NewInt(5))
	prefixSuffixSize.Add(prefixSuffixSize, big.NewInt(5))

	prefix := make([]byte, prefixSuffixSize.Int64())
	_, err = rand.Read(prefix)
	if err != nil {
		return nil, -1, err
	}

	suffix := make([]byte, prefixSuffixSize.Int64())
	_, err = rand.Read(suffix)
	if err != nil {
		return nil, -1, err
	}

	plaintext := append(prefix, b...)
	plaintext = append(plaintext, suffix...)

	padded := challenge_9.PadPKCS7(plaintext, KeySize)

	randKey := make([]byte, KeySize)
	_, err = rand.Read(randKey)
	if err != nil {
		return nil, -1, err
	}

	modeNum, _ := rand.Int(rand.Reader, big.NewInt(2))
	mode = modeNum.Int64()
	switch mode {
	case ECBMode:
		encrypted = challenge_7.EncryptAES_ECB(padded, randKey)
	case CBCMode:
		iv := make([]byte, KeySize)
		_, err = rand.Read(iv)
		if err != nil {
			return nil, -1, err
		}

		encrypted = challenge_10.Encrypt_AES_CBC(padded, randKey, iv)
	}

	return encrypted, mode, nil
}

type EncryptFunc func([]byte) []byte

func DetectMode(f EncryptFunc) (mode int64) {
	b := make([]byte, KeySize*4)
	encrypted := f(b)
	if bytes.Equal(encrypted[KeySize:KeySize*2], encrypted[KeySize*2:KeySize*3]) {
		return ECBMode
	} else {
		return CBCMode
	}
}
