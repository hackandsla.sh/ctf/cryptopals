package challenge_11

import (
	"testing"
)

func TestDetectMode(t *testing.T) {
	numTests := 1000
	correctNum := 0

	for i := 0; i < numTests; i++ {
		var actual int64 = -1
		// We make a small closure here so we can capture the "actual" mode the
		// bytes were encrypted under, then return only the encrypted bytes for
		// the detector function. This result can then be compared to the
		// detector's guess
		var f EncryptFunc = func(b []byte) []byte {
			encrypted, mode, err := EncryptRandomModeWithAnswer(b)
			if err != nil {
				t.Fatalf("%v", err)
			}

			actual = mode
			return encrypted
		}

		guess := DetectMode(f)
		if guess == actual {
			correctNum++
		}
	}

	if correctNum != numTests {
		percentCorrect := float32(correctNum) / float32(numTests) * 100
		t.Errorf("Only got %.0f%% correct", percentCorrect)
	}
}
