package challenge_10

import (
	"encoding/base64"
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDecrypt_AES_CBC(t *testing.T) {
	Assert := assert.New(t)

	encoded, err := ioutil.ReadFile("./encrypted.txt")
	if err != nil {
		t.Fatalf("%v", err)
	}

	encrypted, err := base64.StdEncoding.DecodeString(string(encoded))
	if err != nil {
		t.Fatalf("%v", err)
	}

	expected, err := ioutil.ReadFile("./decrypted.txt")
	if err != nil {
		t.Fatalf("%v", err)
	}

	key := []byte("YELLOW SUBMARINE")
	iv := make([]byte, KeySize)

	decrypted := Decrypt_AES_CBC(encrypted, key, iv)
	Assert.Equal(expected, decrypted)
}

func TestEncrypt_AES_CBC(t *testing.T) {
	Assert := assert.New(t)

	encoded, err := ioutil.ReadFile("./encrypted.txt")
	if err != nil {
		t.Fatalf("%v", err)
	}

	expected, err := base64.StdEncoding.DecodeString(string(encoded))
	if err != nil {
		t.Fatalf("%v", err)
	}

	plaintext, err := ioutil.ReadFile("./decrypted.txt")
	if err != nil {
		t.Fatalf("%v", err)
	}

	key := []byte("YELLOW SUBMARINE")
	iv := make([]byte, KeySize)

	encrypted := Encrypt_AES_CBC(plaintext, key, iv)
	Assert.Equal(expected, encrypted)
}
