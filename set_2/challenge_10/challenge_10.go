package challenge_10

import (
	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_2"
	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_7"
)

const KeySize = 16

func Decrypt_AES_CBC(b, key, iv []byte) []byte {
	bWithIV := append(iv, b...)
	decrypted := challenge_7.DecryptAES_ECB(b, key)

	return challenge_2.XORBytes(decrypted, bWithIV)
}

func Encrypt_AES_CBC(b, key, iv []byte) []byte {
	xorBlock := iv
	encrypted := []byte{}

	for i := 0; i < len(b); i += KeySize {
		block := b[i : i+KeySize]
		xored := challenge_2.XORBytes(block, xorBlock)

		encryptedBlock := challenge_7.EncryptAES_ECB(xored, key)
		xorBlock = encryptedBlock
		encrypted = append(encrypted, encryptedBlock...)
	}

	return encrypted
}
