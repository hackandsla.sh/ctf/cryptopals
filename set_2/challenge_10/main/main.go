package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_10"
)

func main() {
	key := []byte("YELLOW SUBMARINE")
	iv := make([]byte, challenge_10.KeySize)
	encoded, err := ioutil.ReadFile("../encrypted.txt")
	if err != nil {
		log.Fatalf("%v", err)
	}

	encrypted, err := base64.StdEncoding.DecodeString(string(encoded))
	if err != nil {
		log.Fatalf("%v", err)
	}

	decrypted := challenge_10.Decrypt_AES_CBC(encrypted, key, iv)
	fmt.Println(string(decrypted))
}
