package challenge_9

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPadPKCS7(t *testing.T) {
	Assert := assert.New(t)

	type args struct {
		s         string
		blockSize int
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "Test 1",
			args: args{
				s:         "YELLOW SUBMARINE",
				blockSize: 20,
			},
			want: []byte("YELLOW SUBMARINE\x04\x04\x04\x04"),
		},
		{
			name: "Equal size block",
			args: args{
				s:         "FOO",
				blockSize: 3,
			},
			want: []byte("FOO\x03\x03\x03"),
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			got := PadPKCS7([]byte(tt.args.s), tt.args.blockSize)

			Assert.Equal(tt.want, got)
		})
	}
}

func TestUnpadPKCS7(t *testing.T) {
	Assert := assert.New(t)

	tests := []struct {
		name     string
		message  []byte
		expected []byte
		wantErr  bool
	}{
		{
			name:     "Test 1",
			message:  []byte("YELLOW SUBMARINE\x04\x04\x04\x04"),
			expected: []byte("YELLOW SUBMARINE"),
		},
		{
			name:     "Equal size block",
			message:  []byte("FOO\x03\x03\x03"),
			expected: []byte("FOO"),
		},
		{
			name:     "Invalid padding",
			message:  []byte("FOO\x03\x02\x03"),
			expected: nil,
			wantErr:  true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			output, err := UnpadPKCS7(tt.message)

			Assert.Equal(tt.wantErr, err != nil)
			Assert.Equal(tt.expected, output)
		})
	}
}
