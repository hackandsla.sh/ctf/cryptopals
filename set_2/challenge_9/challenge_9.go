package challenge_9

import "errors"

var (
	ErrPadding = errors.New("invalid padding")
)

func PadPKCS7(b []byte, blockSize int) []byte {
	remainder := blockSize - (len(b) % blockSize)

	for i := 0; i < remainder; i++ {
		b = append(b, byte(remainder))
	}

	return b
}

func UnpadPKCS7(b []byte) ([]byte, error) {
	lastByte := b[len(b)-1]
	for i := byte(0); i < lastByte; i++ {
		if b[byte(len(b))-i-1] != lastByte {
			return nil, ErrPadding
		}
	}

	return b[:byte(len(b))-lastByte], nil
}
