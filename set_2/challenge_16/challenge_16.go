package challenge_16

import (
	"crypto/rand"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_10"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_15"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_9"
)

const BlockSize = 16

func getQuery(s string) string {
	s = url.QueryEscape(s)

	return "comment1=cooking%20MCs;userdata=" +
		s + ";comment2=%20like%20a%20pound%20of%20bacon"
}

func isAdmin(q string) bool {
	split := strings.Split(q, ";")

	m := map[string]string{}
	for _, item := range split {
		kv := strings.Split(item, "=")
		m[kv[0]] = kv[1]
	}

	if isAdmin, ok := m["admin"]; ok {
		a, err := strconv.ParseBool(isAdmin)
		return err == nil && a
	}

	return false
}

type Encryptor struct {
	key []byte
}

func NewEncryptor() *Encryptor {
	ret := &Encryptor{
		key: make([]byte, BlockSize),
	}

	_, _ = rand.Read(ret.key)
	return ret
}

func (e *Encryptor) GetQuery(s string) (query, iv []byte) {
	s = getQuery(s)

	return e.encrypt([]byte(s))
}

func (e *Encryptor) IsAdmin(query, iv []byte) (bool, error) {
	plaintext, err := e.decrypt(query, iv)
	if err != nil {
		return false, err
	}

	return isAdmin(string(plaintext)), nil
}

func (e *Encryptor) decrypt(ciphertext, iv []byte) ([]byte, error) {
	decrypted := challenge_10.Decrypt_AES_CBC(ciphertext, e.key, iv)

	if err := challenge_15.ValidatePadding(decrypted); err != nil {
		return nil, err
	}

	unpadded, err := challenge_9.UnpadPKCS7(decrypted)
	if err != nil {
		return nil, err
	}

	return unpadded, nil
}

func (e *Encryptor) encrypt(plaintext []byte) (ciphertext, iv []byte) {
	padded := challenge_9.PadPKCS7(plaintext, BlockSize)

	iv = make([]byte, BlockSize)
	_, _ = rand.Read(iv)

	encrypted := challenge_10.Encrypt_AES_CBC(padded, e.key, iv)

	return encrypted, iv
}
