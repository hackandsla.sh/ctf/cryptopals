package main

import (
	"encoding/hex"
	"fmt"
	"log"
	"strings"

	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_16"
)

func main() {
	e := challenge_16.NewEncryptor()

	prefix := "comment1=cooking%20MCs;userdata="

	// Determine the number of leading characters we need to start our real
	// input on a block
	leadingA := make([]byte, challenge_16.BlockSize-len(prefix)%challenge_16.BlockSize)
	fillSlice(leadingA, 0x41)

	fmt.Printf("LeadingA: %v\n", hex.Dump(leadingA))

	// Create block number 1 with all As
	block1 := make([]byte, challenge_16.BlockSize)
	fillSlice(block1, 0x41)

	fmt.Printf("Block 1: %v\n", hex.Dump(block1))

	// Create our special string to get us admin, and swap out the control
	// characters with normal ones.
	adminString := ";admin=true"
	specialCharMap := map[string]string{
		";": "b",
		"=": "c",
	}
	for char, substitute := range specialCharMap {
		adminString = strings.ReplaceAll(adminString, char, substitute)
	}

	// Create block 2, which has our target for modification.
	block2 := make([]byte, challenge_16.BlockSize-len(adminString)%challenge_16.BlockSize)
	fillSlice(block2, 0x41)

	block2 = append(block2, []byte(adminString)...)

	fmt.Printf("Block 2: %v\n", hex.Dump(block2))

	// Find out how to modify the ciphertext's block1 to transform block2's
	// special characters back to their original form
	mask := make([]byte, challenge_16.BlockSize)
	for char, substitute := range specialCharMap {
		// Get the bitwise difference between these characters and insert it
		// into the mask.
		diff := []byte(char)[0] ^ []byte(substitute)[0]

		index := strings.Index(string(block2), substitute)
		mask[index] = diff
	}

	fmt.Printf("Mask: %v\n", hex.Dump(mask))

	input := append(leadingA, block1...)
	input = append(input, block2...)

	ciphertext, iv := e.GetQuery(string(input))

	block1Num := len(prefix)/challenge_16.BlockSize + 1
	block1Index := block1Num * challenge_16.BlockSize

	// Flip all the bits in block1 according to the mask we constructured
	// earlier.
	for i := 0; i < len(mask); i++ {
		ciphertext[i+block1Index] = ciphertext[i+block1Index] ^ mask[i]
	}

	// Do our final check to determine whether we're admin.
	isAdmin, err := e.IsAdmin(ciphertext, iv)
	if err != nil {
		log.Fatalf("Got error: %v", err)
	}

	if isAdmin {
		fmt.Println("Got admin!")
	} else {
		fmt.Println("Didn't get admin :(")
	}
}

func fillSlice(b []byte, char byte) {
	for i := 0; i < len(b); i++ {
		b[i] = char
	}
}
