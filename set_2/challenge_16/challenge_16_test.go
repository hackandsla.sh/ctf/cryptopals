package challenge_16

import (
	"testing"
)

func TestGetQuery(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Should quote out special characters",
			args: args{
				s: ";admin=true",
			},
			want: "comment1=cooking%20MCs;userdata=%3Badmin%3Dtrue;comment2=%20like%20a%20pound%20of%20bacon",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getQuery(tt.args.s); got != tt.want {
				t.Errorf("GetQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsAdmin(t *testing.T) {
	type args struct {
		q string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Should not indicate user is admin if they embed ';admin=true' in their query",
			args: args{
				q: "comment1=cooking%20MCs;userdata=%3Badmin%3Dtrue;comment2=%20like%20a%20pound%20of%20bacon",
			},
			want: false,
		},
		{
			name: "Should indicate user is admin if it's valid",
			args: args{
				q: "comment1=cooking%20MCs;userdata=%3Badmin%3Dtrue;admin=true;comment2=%20like%20a%20pound%20of%20bacon",
			},
			want: true,
		},
		{
			name: "Should not indicate user is admin if the value isn't valid",
			args: args{
				q: "comment1=cooking%20MCs;userdata=%3Badmin%3Dtrue;admin=foo;comment2=%20like%20a%20pound%20of%20bacon",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isAdmin(tt.args.q); got != tt.want {
				t.Errorf("IsAdmin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEncryptor(t *testing.T) {
	e := NewEncryptor()

	original := "comment1=cooking%20MCs;userdata=%3Badmin%3Dtrue;admin=true;comment2=%20like%20a%20pound%20of%20bacon"

	encrypted, iv := e.encrypt([]byte(original))
	decrypted, err := e.decrypt(encrypted, iv)
	if err != nil {
		t.Errorf("Got err '%v'", err)
	}

	if string(decrypted) != original {
		t.Errorf("Expected '%v', got '%v'", original, decrypted)
	}
}
