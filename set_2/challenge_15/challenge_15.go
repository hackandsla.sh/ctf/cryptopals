package challenge_15

import "errors"

var ErrInvalidPadding = errors.New("invalid padding")

func ValidatePadding(b []byte) error {
	lastByte := b[len(b)-1]
	if lastByte == 0 {
		return ErrInvalidPadding
	}

	for i := 0; i < int(lastByte); i++ {
		value := b[len(b)-i-1]
		if value != lastByte {
			return ErrInvalidPadding
		}
	}

	return nil
}
