package challenge_15

import "testing"

func TestValidatePadding(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Valid padding",
			args: args{
				b: []byte("ICE ICE BABY\x04\x04\x04\x04"),
			},
			wantErr: false,
		},
		{
			name: "Invalid padding 1",
			args: args{
				b: []byte("ICE ICE BABY\x05\x05\x05\x05"),
			},
			wantErr: true,
		},
		{
			name: "Invalid padding 2",
			args: args{
				b: []byte("ICE ICE BABY\x01\x02\x03\x04"),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidatePadding(tt.args.b); (err != nil) != tt.wantErr {
				t.Errorf("ValidatePadding() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
