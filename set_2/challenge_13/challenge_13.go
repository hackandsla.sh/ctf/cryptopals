package challenge_13

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_7"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_9"
)

func MarshalMap(s string) map[string]string {
	kvPairs := strings.Split(s, "&")

	m := make(map[string]string, len(kvPairs))

	for _, pair := range kvPairs {
		splitPair := strings.SplitN(pair, "=", 2)
		m[splitPair[0]] = splitPair[1]
	}

	return m
}

func GetProfile(email string) (string, error) {
	// if err := validation.Validate(email, is.EmailFormat); err != nil {
	// 	return "", err
	// }

	if strings.ContainsAny(email, "&=") {
		return "", errors.New("invalid email")
	}

	return fmt.Sprintf("email=%s&uid=10&role=user", email), nil
}

type ProfileEncrypter struct {
	key []byte
}

func NewProfileEncrypter() (*ProfileEncrypter, error) {
	key, err := GetRandBytes(32)
	if err != nil {
		return nil, err
	}

	return &ProfileEncrypter{
		key: key,
	}, nil
}

func (e *ProfileEncrypter) GetEncryptedProfile(email string) (string, error) {
	if len(e.key) != 32 {
		return "", errors.New("invalid key")
	}

	profile, err := GetProfile(email)
	if err != nil {
		return "", err
	}

	padded := challenge_9.PadPKCS7([]byte(profile), 32)

	encrypted := challenge_7.EncryptAES_ECB(padded, e.key)
	return base64.StdEncoding.EncodeToString(encrypted), nil
}

func (e *ProfileEncrypter) UnencryptProfile(ciphertext string) (map[string]string, error) {
	decoded, err := base64.StdEncoding.DecodeString(ciphertext)
	if err != nil {
		return nil, err
	}

	plaintext := challenge_7.DecryptAES_ECB(decoded, e.key)

	unpadded, err := challenge_9.UnpadPKCS7(plaintext)
	if err != nil {
		return nil, err
	}

	return MarshalMap(string(unpadded)), nil
}

func GetRandBytes(size int) ([]byte, error) {
	b := make([]byte, size)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}
