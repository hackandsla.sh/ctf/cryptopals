package main

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"

	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_13"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_9"
)

const BlockSize int = 32

func main() {
	// Determine how many leading "A" characters to use to align the blocksize
	// to right after the "." character.
	leadingA := make([]byte, BlockSize-len("email=@example."))
	for i := 0; i < len(leadingA); i++ {
		leadingA[i] = 0x41
	}

	// Pad the "admin" to be a full valid PKCS7-padded block
	tld := "admin"
	paddedTLD := challenge_9.PadPKCS7([]byte(tld), BlockSize)

	// Construct the malformed input using the leading "A" characters and the
	// malformed TLD.
	email := append(leadingA, []byte("@example.")...)
	email = append(email, paddedTLD...)

	fmt.Printf("Input: %v\n", string(email))

	e, err := challenge_13.NewProfileEncrypter()
	if err != nil {
		log.Fatalf("%v", err)
	}

	encrypted, err := e.GetEncryptedProfile(string(email))
	if err != nil {
		log.Fatalf("%v", err)
	}

	fmt.Printf("Encrypted: %s\n", encrypted)
	b, err := base64.StdEncoding.DecodeString(encrypted)
	if err != nil {
		log.Fatalf("%v", err)
	}

	fmt.Println(hex.Dump(b))

	// Copy the block which contains the "admin" + PKCS7 padding
	adminBlock := b[BlockSize : BlockSize*2]

	// Determine the number of leading "A" characters to align the "role=" to be
	// right at the end of a valid block.
	leadingA = make([]byte, BlockSize-len("email=@example.com&uid=10&role="))
	for i := 0; i < len(leadingA); i++ {
		leadingA[i] = 0x41
	}

	// Construct the email input
	email2 := string(leadingA) + "@example.com"
	fmt.Printf("Input 2: %s\n", email2)

	encrypted2, err := e.GetEncryptedProfile(email2)
	if err != nil {
		log.Fatalf("%v", err)
	}

	b2, err := base64.StdEncoding.DecodeString(encrypted2)
	if err != nil {
		log.Fatalf("%v", err)
	}

	fmt.Println(hex.Dump(b2))

	// Insert the block contains "admin" at the end of our newly-encrypted item.
	for i := 0; i < len(adminBlock); i++ {
		b2[i+BlockSize] = adminBlock[i]
	}

	encoded := base64.StdEncoding.EncodeToString(b2)
	profile, err := e.UnencryptProfile(encoded)
	if err != nil {
		log.Fatalf("%v", err)
	}

	fmt.Printf("Profile: %v\n", profile)
	if profile["role"] == "admin" {
		fmt.Println("Successfully created an admin account")
	} else {
		fmt.Printf("Expected role 'admin', got '%s'\n", profile["role"])
	}
}
