package challenge_13

import (
	"reflect"
	"testing"
)

func TestMarshalMap(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want map[string]string
	}{
		{
			name: "Test 1",
			args: args{
				s: "foo=bar&baz=qux&zap=zazzle",
			},
			want: map[string]string{
				"foo": "bar",
				"baz": "qux",
				"zap": "zazzle",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MarshalMap(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MarshalMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetProfile(t *testing.T) {
	type args struct {
		email string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Valid input",
			args: args{email: "foo@example.com"},
			want: "email=foo@example.com&uid=10&role=user",
		},
		{
			name:    "Invalid format",
			args:    args{email: "fooexample.com"},
			wantErr: true,
		},
		{
			name:    "Invalid characters",
			args:    args{email: "fo=o@example.com"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetProfile(tt.args.email)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetProfile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetProfile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestProfileEncrypter(t *testing.T) {
	type args struct {
		email string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Valid Email",
			args: args{
				email: "foo@example.com",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e, err := NewProfileEncrypter()
			if err != nil {
				t.Fatalf("%v", err)
			}

			encrypted, err := e.GetEncryptedProfile(tt.args.email)
			if err != nil {
				t.Fatalf("%v", err)
			}

			decrypted, err := e.UnencryptProfile(encrypted)
			if err != nil {
				t.Fatalf("%v", err)
			}

			if decrypted["email"] != tt.args.email {
				t.Errorf("expected %s, got %s", tt.args.email, decrypted["email"])
			}

			if decrypted["uid"] != "10" {
				t.Errorf("expected uid 10, got %s", decrypted["uid"])
			}

			if decrypted["role"] != "user" {
				t.Errorf("expected role 'user', got '%s'", decrypted["role"])
			}
		})
	}
}
