package main

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/terrabitz/ctf/cryptopals/set_1/challenge_7"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_11"
	"gitlab.com/terrabitz/ctf/cryptopals/set_2/challenge_9"
)

func main() {
	key := make([]byte, 16)
	_, err := rand.Read(key)
	if err != nil {
		log.Fatalf("%v", err)
	}

	unknownText, err := ioutil.ReadFile("unknown.txt")
	if err != nil {
		log.Fatalf("%v", err)
	}
	unknownText, _ = base64.StdEncoding.DecodeString(string(unknownText))

	var oracle challenge_11.EncryptFunc = func(b []byte) []byte {
		plaintext := append(b, unknownText...)
		plaintext = challenge_9.PadPKCS7(plaintext, len(key))
		return challenge_7.EncryptAES_ECB(plaintext, key)
	}

	// START TESTS
	blockSize, err := FindECBBlockSize(oracle)
	if err != nil {
		log.Fatalf("%v", err)
	}
	fmt.Printf("Found block size: %v\n", blockSize)

	mode := challenge_11.DetectMode(oracle)
	if mode == challenge_11.ECBMode {
		fmt.Println("Found mode: ECB")
	} else {
		log.Fatal("unknown mode\n")
	}

	c := oracle([]byte{})
	blockStart := (len(c) - 1) / blockSize * blockSize
	secretMessage := []byte{}
	for i := 0; i < len(c)-1; i++ {
		prefix := make([]byte, len(c)-i-1)
		for j := 0; j < len(prefix); j++ {
			prefix[j] = 0x41
		}

		c := oracle(prefix)

		targetBlock := c[blockStart : blockStart+blockSize]

		for j := byte(0); j < 255; j++ {
			guessPrefix := append(prefix, secretMessage...)
			guessPrefix = append(guessPrefix, j)
			c2 := oracle(guessPrefix)

			guessBlock := c2[blockStart : blockStart+blockSize]
			if bytes.Equal(targetBlock, guessBlock) {
				secretMessage = append(secretMessage, j)
				break
			}
		}
	}

	fmt.Printf("Secret message found:\n\n%s\n", string(secretMessage))
}

func FindECBBlockSize(oracle challenge_11.EncryptFunc) (int, error) {
	i := 0
	max := 100
	for {
		i++
		plaintext := make([]byte, i)
		for j := 0; j < i; j++ {
			plaintext[j] = []byte("A")[0]
		}

		c := oracle(plaintext)

		plaintext2 := append(plaintext, []byte("A")...)
		c2 := oracle(plaintext2)
		if bytes.Equal(c[:i], c2[:i]) {
			return i, nil
		}

		if i == max {
			return 0, errors.New("couldn't find block size")
		}
	}
}
